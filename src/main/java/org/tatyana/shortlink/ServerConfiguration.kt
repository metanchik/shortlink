package org.tatyana.shortlink

import arrow.syntax.option.toOption
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import io.ktor.application.Application
import io.ktor.application.ApplicationCall
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.content.*
import io.ktor.pipeline.PipelineContext
import io.ktor.request.receiveText
import io.ktor.response.respondText
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.sessions.Sessions
import io.ktor.sessions.cookie
import io.ktor.sessions.get
import io.ktor.sessions.sessions
import org.tatyana.shortlink.service.DoAction
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.UserSession
import org.tatyana.shortlink.pages.*
import org.tatyana.shortlink.service.Actions

fun Application.modulez() {
    val actions = Actions()
    val staticFilesDir = ShortLink.getStaticFilesDir()

    install(Sessions) {
        cookie<UserSession.Cookie>("UserSession")
    }

    routing {
        get("/login") {
            LoginPage().respond(call)
        }

        get("/registration") {
            RegistrationPage().respond(call)
        }

        getAuthorized("/") { user ->
            MainPage(user).respond(call)
        }

        post("/doLogin") {
            val bodyJson = Parser().parse(StringBuilder(call.receiveText())) as JsonObject

            val login = bodyJson.get("login") as String
            val password = bodyJson.get("password") as String

            val res = actions.loginUser(call, login, password)
//            RedirectPage(redirect).respond(call)
            call.respondText { res }
        }

        post("/doRegistration") {
            val bodyJson = Parser().parse(StringBuilder(call.receiveText())) as JsonObject

            val login = bodyJson.get("login") as String
            val password = bodyJson.get("password") as String

            val res = actions.registrationUser(call, login, password)
//            RedirectPage(redirect).respond(call)
            call.respondText { res }
        }

        get("/error") {
            ErrorPage().respond(call)
        }

        get("/sl/{shortLink}") {
            val shortlinkPart = call.parameters.get("shortLink")!!
            actions.getFullLinkByShortPart(shortlinkPart).respond(call)
        }

        postAuthorized("/shorten") {user ->
            val bodyJson = Parser().parse(StringBuilder(call.receiveText())) as JsonObject
            val link = bodyJson.get("link") as String
            val res = actions.createShortLink(user, link)
            call.respondText { res }
        }

        getAuthorized("/logout") {user ->
            actions.logout(call)
            RedirectPage("/login").respond(call)
        }

        getAuthorized("/auth") { user ->
            call.respondText { "Auth by name: ${user.name}" }
        }

        getAuthorized("/userStatistics") {user ->
            val links = actions.getUserStatistics(user)
            UserStatisticsPage(user, links).respond(call)
        }

        getAuthorized("/statistics") {user ->
            val usersLinks = actions.getStatistics()
            StatisticsPage(user, usersLinks).respond(call)
        }

        static("/static") {
            ShortLink.staticRootFolder = staticRootFolder.toOption()
            staticBasePackage = "static"
            defaultResource("index.html")
            files(staticFilesDir)
        }
    }
}


fun Route.getAuthorized(path: String, action: suspend PipelineContext<Unit, ApplicationCall>.(UserData) -> Unit): Route {
    return get(path) {
        authorized { userId -> action(this, userId) }
    }
}

fun Route.postAuthorized(path: String, action: suspend PipelineContext<Unit, ApplicationCall>.(UserData) -> Unit): Route {
    return post(path) {
        authorized { userId -> action(this, userId) }
    }
}

suspend fun PipelineContext<Unit, ApplicationCall>.authorized(action: suspend PipelineContext<Unit, ApplicationCall>.(UserData) -> Unit) {
    val session = call.sessions.get<UserSession.Cookie>()
    session.toOption().fold(
            { RedirectPage("/login") },
            { cookie ->
                ShortLink.actions.getUserIdForSessionCookie(cookie).fold(
                        { RedirectPage("/login") },
                        { userId ->
                            DoAction(this, userId, action)
                        }
                )
            }).respond(call)
}
