package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IAuthorizedPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import org.tatyana.shortlink.service.models.LinkModel
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.template

class UserStatisticsPage(override val user: UserData, val links: MutableList<LinkModel>) : IAuthorizedPage {
    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            template(user, {
                div(classes = "row") {
                    div(classes = "col-md-2") {
                        span{ + "User: ${user.name}"}
                    }
                    div(classes = "col-md-2") {
                        span{ + "Links: ${links.count()}"}
                    }
                    div(classes = "col-md-2") {
                        span{ + "Calls: ${links.sumBy { it.nCalls }}"}
                    }
                }
                hr {  }
                div(classes = "row") {
                    table(classes = "table table-bordered table-hover") {
                        thead {
                            tr {
                                th { +"full link" }
                                th { +"short link" }
                                th { +"nCalls count" }
                            }
                        }
                        tbody {
                            links.forEach {link ->
                                tr {
                                    td { a(href=link.fullLink) {+ link.fullLink}}
                                    td { a(href=link.shortLink) {+ link.shortLink}}
                                    td {+ link.nCalls.toString()}
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}