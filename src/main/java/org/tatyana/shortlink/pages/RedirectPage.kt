package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.response.respondRedirect

class RedirectPage(val url: String) : IPage {
    override suspend fun respond(call: ApplicationCall) {
        println(">> Redirecting to \"$url\"")
        call.respondRedirect(url)
    }
}