package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import org.tatyana.shortlink.service.includeTwitterBootstrap
import org.tatyana.shortlink.service.includeTwitterBootstrapCss

class RegistrationPage : IPage {
    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            head {
                title { +"Registration" }
                includeTwitterBootstrapCss()
                includeTwitterBootstrap()
            }
            body {
                div(classes = "container") {
                    hr {}
                    div(classes = "row") {
                        form(classes = "form") {
                            id = "form"
                            input(type = InputType.text) {
                                id = "login"
                                name = "login"
                                placeholder = "login"
                            }
                            input(type = InputType.password) {
                                id = "password"
                                name = "password"
                                placeholder = "password"
                            }
                            input(type = InputType.password) {
                                id = "confirm_password"
                                name = "confirm_password"
                                placeholder = "confirm password"
                            }
                            a(classes = "btn btn-sm") {
                                id = "login-btn"
                                onClick = "registration()"
                                +"Registration"
                            }
                        }
                    }
                    hr {}
                    div(classes = "row") {
                        a(href = "/login") {
                            id = "login-link"
                            +"Login"
                        }
                    }
                }
                script("text/javascript", "/static/js/registration.js") {}
            }
        }
    }
}