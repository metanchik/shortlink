package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.body
import kotlinx.html.h5
import kotlinx.html.head

class ErrorPage : IPage {
    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            head {  }
            body {
                h5 {+ "An error has occurred. Try again later."}
            }
        }
    }
}