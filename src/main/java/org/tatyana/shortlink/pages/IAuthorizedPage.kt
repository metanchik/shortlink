package com.socialquantum.testing.service.ktor.pages

import org.tatyana.shortlink.service.models.UserData

interface IAuthorizedPage : IPage {
    val user: UserData
}