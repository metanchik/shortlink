package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import org.tatyana.shortlink.service.includeTwitterBootstrap
import org.tatyana.shortlink.service.includeTwitterBootstrapCss

class LoginPage : IPage {

    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            head {
                title { +"Login" }
                includeTwitterBootstrapCss()
                includeTwitterBootstrap()
            }
            body {
                div(classes = "container") {
                    hr {}
                    div(classes = "row") {
                        form(classes = "form") {
                            id = "form"
                            input(type = InputType.text) {
                                id = "login"
                                name = "login"
                                placeholder = "login"
                            }
                            input(type = InputType.password) {
                                id = "password"
                                name = "password"
                                placeholder = "password"
                            }
                            a(classes = "btn btn-sm") {
                                id = "login-btn"
                                onClick = "login()"
                                +"Login"
                            }
                        }
                    }
                    hr {}
                    div(classes = "row") {
                        a(href = "/registration") {
                            id = "registration-link"
                            +"Registration"
                        }
                    }
                }
                script("text/javascript", "/static/js/login.js") {}
            }
        }
    }
}