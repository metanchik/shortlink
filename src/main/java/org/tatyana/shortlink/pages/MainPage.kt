package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IAuthorizedPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.template

class MainPage(override val user: UserData) : IAuthorizedPage {
    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            template(user, {
                div(classes = "row") {
                    div(classes="col-md-2") {
                        span(classes = "") {
                            +"Specify a link"
                        }
                    }
                    div(classes="col-md-8") {
                        input(type = InputType.text, classes = "input-link") {
                            id = "link"
                        }
                    }
                    div(classes="col-md-2") {
                        a(classes = "btn btn-sm short-btn") {
                            //                        onClick = "shorten();"
                            id = "shorten-btn"
                            text("Shorten")
                        }
                    }
                }
                script("text/javascript", "/static/js/shorten.js") {}
            }, false)
        }
    }
}