package com.socialquantum.testing.service.ktor.pages

import io.ktor.application.ApplicationCall

interface IPage {
    suspend fun respond(call: ApplicationCall)
}