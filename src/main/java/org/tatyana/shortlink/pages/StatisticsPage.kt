package org.tatyana.shortlink.pages

import com.socialquantum.testing.service.ktor.pages.IAuthorizedPage
import io.ktor.application.ApplicationCall
import io.ktor.html.respondHtml
import kotlinx.html.*
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.models.UserStatistics
import org.tatyana.shortlink.service.template

class StatisticsPage(override val user: UserData, val userStat: MutableMap<Int, UserStatistics>) : IAuthorizedPage {
    override suspend fun respond(call: ApplicationCall) {
        call.respondHtml {
            template(user, {
                var linksCount = 0
                var callsCount = 0
                userStat.forEach { userId, userModel ->
                    linksCount += userModel.statistics.count()
                    callsCount += userModel.statistics.sumBy { it.nCalls }
                }
                div(classes = "row") {
                    div(classes = "col-md-2") {
                        span { +"Users count: ${userStat.count()}" }
                    }
                    div(classes = "col-md-2") {
                        span { +"Links count: ${linksCount}" }
                    }
                    div(classes = "col-md-2") {
                        span { +"Calls count: ${callsCount}" }
                    }
                }
                hr { }
                div(classes = "row") {
                    div(classes = "") {
                        h3 { +"Total user statistics" }
                    }
                    table(classes = "table table-bordered table-hover") {
                        thead {
                            tr {
                                th { +"User" }
                                th { +"Links count" }
                                th { +"Calls count" }
                            }
                        }
                        tbody {
                            userStat.forEach { userId, userModel ->
                                val userLinks = userModel.statistics.count()
                                val userCalls = userModel.statistics.sumBy { it.nCalls }
                                tr {
                                    td { +userModel.user.name }
                                    td { +userLinks.toString() }
                                    td { +userCalls.toString() }
                                }
                            }
                        }
                    }
                }
                hr { }
                div(classes = "row") {
                    div(classes = "") {
                        h3 { +"Statistics by links" }
                    }
                    table(classes = "table table-bordered table-hover") {
                        thead {
                            tr {
                                th { +"User" }
                                th { +"Full link" }
                                th { +"Short link" }
                                th { +"Calls count" }
                            }
                        }
                        tbody {
                            userStat.forEach { userId, userModel ->
                                userModel.statistics.forEach { linkModel ->
                                    tr {
                                        td { +userModel.user.name }
                                        td { a(href = linkModel.fullLink) { +linkModel.fullLink } }
                                        td { a(href = linkModel.shortLink) { +linkModel.shortLink } }
                                        td { +linkModel.nCalls.toString() }
                                    }
                                }
                            }
                        }
                    }
                }
            })
        }
    }
}