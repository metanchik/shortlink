package org.tatyana.shortlink.service

import org.tatyana.shortlink.service.models.UserData
import java.util.*

class UserSession(val user: UserData, val cookie: Cookie) {

    data class Cookie(val uuid: UUID)

    constructor(user: UserData) : this(user, Cookie(UUID.randomUUID()))

}