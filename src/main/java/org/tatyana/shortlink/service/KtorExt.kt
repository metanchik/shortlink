package org.tatyana.shortlink.service

import kotlinx.html.FlowContent
import kotlinx.html.*
import org.tatyana.shortlink.service.models.UserData

/** The necessary Twitter Bootstrap CSS*/
fun FlowOrPhrasingOrMetaDataContent.includeTwitterBootstrapCss() {
    link("/static/css/bootstrap/v4.0.0/slate-bootstrap.min.css", "stylesheet")
    link("/static/css/bootstrap/bootstrap-fix.css", "stylesheet")
}

fun FlowOrPhrasingOrMetaDataContent.includeTwitterBootstrap() {
    includeTwitterBootstrapCss()
    script("text/javascript", "/static/js/libs/jquery-3.2.1.min.js") {}
    link("/static/css/style.css", "stylesheet")
}

/**---------------------------------------------------------------------------------------------*/

fun FlowContent.infoHeader(user: UserData, isShortenLink: Boolean) {
    hr {}
    div(classes = "container") {
        div(classes = "row") {
            div(classes = "col-md-6") {
                h4 {
                    style = "margin-right:20px;"
                    +"User '${user.name}'"
                }
            }
            div (classes = "col-md-6") {
                a(href = "/logout", classes = "btn btn-sm menu") { +"logout" }
                a(href = "/userStatistics", classes = "btn btn-sm menu") { +"user statistics" }
                a(href = "/statistics", classes = "btn btn-sm menu") { +"statistics" }
                if (isShortenLink) {
                    a(href = "/", classes = "btn btn-sm menu") { + "shorten"}
                }
            }
        }
    }
    hr {}
}

fun HTML.template(user: UserData, bodyContent: FlowContent.() -> Unit, shortenLink: Boolean = true) {
    head {
        includeTwitterBootstrap()
    }
    body {
        infoHeader(user, shortenLink)
        div(classes = "content container") {
            id = "content"
            bodyContent()
        }
        hr {}
    }
}