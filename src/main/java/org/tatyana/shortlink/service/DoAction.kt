package org.tatyana.shortlink.service

import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.pipeline.PipelineContext
import org.tatyana.shortlink.service.models.UserData

class DoAction(val context: PipelineContext<Unit, ApplicationCall>,
               val userId: UserData,
               val action: suspend PipelineContext<Unit, ApplicationCall>.(UserData) -> Unit): IPage {
    suspend override fun respond(call: ApplicationCall) {
        action(context, userId)
    }


}