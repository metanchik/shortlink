package org.tatyana.shortlink.service

import arrow.core.None
import arrow.core.Option
import arrow.syntax.option.some
import org.tatyana.shortlink.service.models.LinkModel
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.models.UserId
import org.tatyana.shortlink.service.models.UserStatistics
import java.sql.Connection
import java.sql.DriverManager
import java.sql.ResultSet
import java.sql.SQLException
import java.sql.Statement


object Db {
    var conn: Connection? = null
    lateinit var statmt: Statement
    lateinit var resSet: ResultSet

    fun connection() {
        conn = null
        Class.forName("org.sqlite.JDBC")
        conn = DriverManager.getConnection("jdbc:sqlite:src/main/resources/db/shortlink.s3db")
    }

    fun createDb() {
        statmt = conn!!.createStatement()
        statmt.execute("CREATE TABLE if not exists 'users' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'login' text UNIQUE, 'password' text);")
        statmt.execute("CREATE TABLE if not exists 'links' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'fullLink' text, 'shortLink' text, 'userId' integer);")
        statmt.execute("CREATE TABLE if not exists 'nCalls' ('id' INTEGER PRIMARY KEY AUTOINCREMENT, 'linkId' text, 'date' integer);")
    }

    fun getUserByLogin(login: String): Option<UserData> {
        try {
            val res = statmt.executeQuery("SELECT * FROM users WHERE users.login = '$login'")
            return UserData(UserId(res.getInt("id")), res.getString("login")).some()
        } catch (e: Exception) {
            println(e)
            return None
        }
    }

    fun getUserByLoginAndPassword(login: String, password: String): Option<UserData> {
        try {
//            statmt.execute("INSERT OR IGNORE INTO users ('login') VALUES('$login');\n")
            val res = statmt.executeQuery("SELECT * FROM users WHERE users.login = '$login' AND users.password = '$password';")
            return UserData(UserId(res.getInt("id")), res.getString("login")).some()
        } catch (e: Exception) {
            println(e)
            return None
        }
    }

    fun addLinkInDb(user: UserData, link: String): Option<ResultSet> {
        try {
            statmt.execute("INSERT OR IGNORE INTO links ('fullLink', 'userId') VALUES ('$link', '${user.id.id}')")
            return getItemFromLinks(user, link)
        } catch (e: Exception) {
            println(e)
            return None
        }
    }

    fun getShortLinkFromDb(user: UserData, link: String): Option<String> {
        val k = getItemFromLinks(user, link)
        val res = k.map { it.getString("shortLink") }
        return res
    }

    fun getItemFromLinks(user: UserData, link: String): Option<ResultSet> {
        try {
            val res = statmt.executeQuery("SELECT * FROM links WHERE userId = ${user.id.id} AND fullLink = '$link'")
            if (res.next()) {
                return res.some()
            } else {
                return None
            }
        } catch (e: Exception) {
            println(e)
            return None
        }
    }

    fun getItemByUserAndShortLink(shortLink: String): Option<ResultSet> {
        try {
            val res = statmt.executeQuery("SELECT * FROM links WHERE shortLink = '$shortLink'")
            if (res.next()) {
                return res.some()
            } else {
                return None
            }
        } catch (e: Exception) {
            println(e)
            return None
        }
    }

    fun addShortLink(id: Int, shortLinkPart: String) {
        try {
            statmt.execute("UPDATE links SET shortLink = '$shortLinkPart' WHERE id = $id")
        } catch (e: Exception) {
            println(e)
        }
    }

    fun addCall(id: Int, date: Long) {
        try {
            statmt.execute("INSERT OR IGNORE INTO nCalls ('linkId', 'date') VALUES ($id, ${date.toInt()})")
        } catch (e: Exception) {
            println(e)
        }
    }

    fun getAllLinksByUser(userId: Int): MutableList<LinkModel> {
        val links = mutableListOf<LinkModel>()
        try {
            val res = statmt.executeQuery("SELECT fullLink, shortLink, COUNT(nCalls.id) as count\n" +
                    "FROM links\n" +
                    "LEFT JOIN nCalls\n" +
                    "ON nCalls.linkId = links.id\n" +
                    "WHERE links.userId = ${userId}\n" +
                    "GROUP BY links.id\n")
                while (res.next()) {
                    links.add(LinkModel(
                            res.getString("fullLink"),
                            res.getString("shortLink"),
                            res.getInt("count")
                    ))
                }
        } catch (e: Exception) {
            println(e)
        }
        return links
    }

    fun getAllLinksByAllUsers(): MutableMap<Int, UserStatistics> {
        val res = mutableMapOf<Int, UserStatistics>()
        try {
            val users = statmt.executeQuery("SELECT * FROM users")
            while (users.next()) {
                val id = users.getInt("id")
                val s = UserStatistics(UserData(UserId(id), users.getString("login")), mutableListOf())
                res.put(id, s)
            }
            val links = statmt.executeQuery("SELECT users.id as userId, fullLink, shortLink, count(nCalls.id) as count\n" +
                    "FROM links\n" +
                    "LEFT JOIN nCalls\n" +
                    "ON nCalls.linkId = links.id\n" +
                    "LEFT JOIN users\n" +
                    "ON users.id = links.userId\n" +
                    "GROUP BY links.id")
            while (links.next()) {
                val statistic = res.get(links.getInt("userId"))!!
                statistic.statistics.add(LinkModel(links.getString("fullLink"), links.getString("shortLink"), links.getInt("count")))
            }
        } catch (e: Exception) {
            println(e)
        }
        return res
    }

    fun insertNewUser(login: String, password: String): Option<UserData> {
        try {
            statmt.execute("INSERT INTO users ('login', 'password') VALUES('$login', '$password');\n")
            val res = statmt.executeQuery("SELECT * FROM users WHERE users.login = '$login' AND users.password = '$password';")
            return UserData(UserId(res.getInt("id")), res.getString("login")).some()
        } catch (e: Exception) {
            println(e)
            return None
        }
    }
}