package org.tatyana.shortlink.service

import arrow.core.None
import arrow.core.Option
import arrow.syntax.option.some
import arrow.syntax.option.toOption
import com.beust.klaxon.json
import com.socialquantum.testing.service.ktor.pages.IPage
import io.ktor.application.ApplicationCall
import io.ktor.sessions.sessions
import org.apache.commons.validator.routines.UrlValidator
import org.hashids.Hashids
import org.tatyana.shortlink.ShortLink
import org.tatyana.shortlink.pages.RedirectPage
import org.tatyana.shortlink.service.models.LinkModel
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.models.UserStatistics
import java.util.regex.Pattern

class Actions {
    val schemes = arrayListOf<String>("http", "https", "ftp")
    fun loginUser(call: ApplicationCall, login: String, password: String): String {
        val userData = Db.getUserByLoginAndPassword(login, password)

        userData.fold(
                { return json{obj("status" to "error", "error" to "Incorrect login or password")}.toJsonString() },
                { user ->
                    val session = UserSession(user)
                    call.sessions.set("UserSession", session.cookie)
                    ShortLink.UserSessions.put(session.cookie.uuid, user)
                    return json{ obj("status" to "ok", "redirect" to "/")}.toJsonString()
                }
        )

    }

    fun registrationUser(call: ApplicationCall, login: String, password: String): String {
        val userData = Db.getUserByLogin(login)

        userData.fold(
                {
                    val createdUser = Db.insertNewUser(login, password)
                    createdUser.fold(
                            { return json { obj("status" to "error", "error" to "The user was not created. Try later.") }.toJsonString() },
                            { newUser ->
                                val session = UserSession(newUser)
                                call.sessions.set("UserSession", session.cookie)
                                ShortLink.UserSessions.put(session.cookie.uuid, newUser)
                                return json { obj("status" to "ok", "redirect" to "/") }.toJsonString()
                            })
                },
                { user ->
                    return json{obj("status" to "error", "error" to "User with such login exists")}.toJsonString()
                }
        )
    }

    fun getUserIdForSessionCookie(cookie: UserSession.Cookie): Option<UserData> {
        return ShortLink.UserSessions.get(cookie.uuid).toOption()
    }

    fun logout(call: ApplicationCall) {
        call.sessions.clear("UserSession")
    }

    fun getUrl(link: String): String {
        val m = Pattern.compile("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?").matcher(link)
        if (!m.matches()) {
            println("MATCH FAIl")
        }
        val scheme = m.group(2)
        if (!schemes.contains(scheme)) {
            return "http://" + link
        }
        return link
    }

    fun createShortLink(user: UserData, link: String): String {
        val urlLink = getUrl(link)
        val isUrl = UrlValidator.getInstance().isValid(urlLink)
        return if (isUrl) {
            Db.getShortLinkFromDb(user, urlLink).fold(
                    {
                        println("added full link in db")
                        generateShortLink(user, urlLink).fold(
                                {
                                    json { obj("status" to "error", "error" to "An error has occurred. Try again later") }.toJsonString()
                                },
                                { shortLink ->
                                    json { obj("status" to "ok", "shortLink" to shortLink) }.toJsonString()
                                }
                        )
                    },
                    { shortLink ->
                        json { obj("status" to "ok", "shortLink" to shortLink) }.toJsonString()
                    }
            )
        } else {
            json { obj("status" to "error", "error" to "Incorrect url") }.toJsonString()
        }
    }

    fun generateShortLink(user: UserData, link: String): Option<String> {
        return Db.addLinkInDb(user, link).fold(
                { None },
                { res ->
                    println("GENERATE LINK")
                    val id = res.getInt("id")
//                    val shortLinkPart = Integer.toString(id, 36)
                    val hashids = Hashids(System.currentTimeMillis().toString())
                    val shortLinkPart = hashids.encode(id.toLong())
                    val shortLink = "http://${ShortLink.domainName}/sl/$shortLinkPart"
                    Db.addShortLink(id, shortLink)
                    shortLink.some()
                }
        )
    }

    fun getFullLinkByShortPart(shortLinkPart: String): IPage {
        val shortLink = "http://${ShortLink.domainName}/sl/$shortLinkPart"
        return Db.getItemByUserAndShortLink(shortLink).fold(
                { RedirectPage("/error") },
                { res ->
                    val id = res.getInt("id")
                    val fullLink = res.getString("fullLink")
                    val date = System.currentTimeMillis()
                    Db.addCall(id, date)
                    RedirectPage(fullLink)
                }
        )
    }

    fun getUserStatistics(user: UserData): MutableList<LinkModel> {
        return Db.getAllLinksByUser(user.id.id)
    }

    fun getStatistics(): MutableMap<Int, UserStatistics> {
        return Db.getAllLinksByAllUsers()
    }
}