package org.tatyana.shortlink

import arrow.core.None
import arrow.core.Option
import io.ktor.application.Application
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import org.tatyana.shortlink.service.Db
import org.tatyana.shortlink.service.models.UserData
import org.tatyana.shortlink.service.Actions
import java.io.File
import java.util.*

object ShortLink {
    var staticRootFolder: Option<File> = None
    val domainName = "127.0.0.1:8080"

    val UserSessions = mutableMapOf<UUID, UserData>()
    val actions = Actions()

    fun getStaticFilesDir(): File {
        val staticFilesDir = File("src/main/resources/static")
        println("static dir = ${staticFilesDir.absolutePath}")
        return staticFilesDir
    }

    @JvmStatic
    fun main(args: Array<String>) {
        Db.connection()
        Db.createDb()

        val server = embeddedServer(Netty, port = 8080, module = Application::modulez)
        server.start(wait = true)

    }
}