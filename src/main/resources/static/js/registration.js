
function registration() {
    var valid = validate();

    if (valid) {
        var url = "/doRegistration";
        var login = $('input#login').val();
        var password = $('input#password').val();
        var jsonBody = {
             "login": login,
             "password": password
        };
        var stringBody = JSON.stringify(jsonBody);
        fetch(url, {
            method: "POST",
            body: stringBody
        }).then(function(response) {
            if (response.redirected == true) {
                document.location = response.url;
            } else {
                response.json().then(function(json) {
                    switch(json["status"]) {
                        case "ok":
                            var result = json["redirect"];
                            window.location = result;

                            break;
                        case "error":
                            var result = json["error"];
                            viewError(result);
                            break;
                    }
                });
            }
        });
    }
}

function resetError() {
    $("#error-div").detach();
}

function validate() {
    var elems = form.elements;
    var valid = true;
    resetError();

    if (!elems.login.value) {
        viewError(' Enter login');
        valid = false;
    }

    if (!elems.password.value) {
        viewError(' Enter password');
        valid = false;
    } else if (elems.password.value != elems.confirm_password.value) {
        viewError(' Passwords do not match');
        valid = false;
    }

    return valid;
}

function viewError(errorText) {
    var errorDiv = document.getElementById("error-div");
    if (errorDiv == null) {
        var content = document.getElementById("form");
        var errorDiv = document.createElement("div");
        errorDiv.setAttribute("id", "error-div");
        content.appendChild(errorDiv);
    }

    var h5 = document.createElement("h5");
    h5.textContent = errorText;
    h5.setAttribute("style", "color:red; text-align: center;")
    errorDiv.appendChild(h5);
}