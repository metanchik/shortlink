/** взято с https://learn.javascript.ru/cookie */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

$(".input-link").focus(function() {
    $(this).select();
});

document.getElementById("link").onkeyup = function(event) {
    event = event || window.event;
    if ((event.keyCode == 0xA)||(event.keyCode == 0xD)) {
        shortenLink();
        return false;
    }
};

var sb = document.getElementById("shorten-btn");
sb.onclick = shortenLink;

function shortenLink() {
    $("#short-link-div").detach();
    $("#error-row").detach();

    var url = "/shorten";
    var link = $('input#link').val();
    var cookie = getCookie("UserSession").replace("uuid=%23s", "");
    var jsonBody = {
         "link": link
    };
    var stringBody = JSON.stringify(jsonBody);
    fetch(url, {
        method: "POST",
        headers: {
            "User-Cookie": cookie
        },
        body: stringBody
    }).then(function(response) {
        if (response.redirected == true) {
            document.location = response.url;
        } else {
            response.json().then(function(json) {
                switch(json["status"]) {
                    case "ok":
                        var result = json["shortLink"];
                        console.log(result);
                        viewShortLink(result);

                        break;
                    case "error":
                        var result = json["error"];
                        viewError(result);
                        break;
                }
            });
        }
    });
}

function viewShortLink(shortenLink) {
    var content = document.getElementById("content");
    var shortLink = document.createElement("div");
    shortLink.setAttribute("id", "short-link-div");
    shortLink.setAttribute("class", "row");
    var span = document.createElement("span");
    span.textContent = "Short link";
    var spanDiv = document.createElement("div");
    spanDiv.setAttribute("class", "col-md-2");
    spanDiv.appendChild(span);

    var input = document.createElement("input");
    input.setAttribute("id", "short-link");
    input.setAttribute("type", "text");
    input.setAttribute("class", "input-link");
    input.value = shortenLink;
    var inputDiv = document.createElement("div");
    inputDiv.setAttribute("class", "col-md-8");
    inputDiv.appendChild(input);

    var btn = document.createElement("a");
    btn.setAttribute("class", "btn btn-sm short-btn");
    btn.text = "Copy";
    var btnDiv = document.createElement("div");
    btnDiv.setAttribute("class", "col-md-2");
    btnDiv.appendChild(btn);

    shortLink.appendChild(spanDiv);
    shortLink.appendChild(inputDiv);
    shortLink.appendChild(btnDiv);
    content.appendChild(shortLink);

    $("#short-link").focus(function() {
        $(this).select();
    });

    btn.onclick = function() {
        input.focus();
        input.select();
        var success = document.execCommand('Copy');
    };

    input.focus();
}

function viewError(errorText) {
    var content = document.getElementById("content");
    var rowDiv = document.createElement("div");
    rowDiv.setAttribute("class", "row");
    rowDiv.setAttribute("id", "error-row");
    var div = document.createElement("div");
    div.setAttribute("class", "col-md-2");
    var errorDiv = document.createElement("div");
    errorDiv.setAttribute("id", "error-div");
    errorDiv.setAttribute("class", "col-md-8");
    var h3 = document.createElement("h3");
    h3.textContent = errorText;
    h3.setAttribute("style", "color:red;")
    errorDiv.appendChild(h3);
    rowDiv.appendChild(div);
    rowDiv.appendChild(errorDiv);
    content.appendChild(rowDiv);
}